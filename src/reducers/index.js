import auth from './auth'
import categories from './categories'
import links from './links'

export default {
  auth,
  categories,
  links,
}
