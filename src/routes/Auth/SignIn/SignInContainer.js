import React from 'react'
import { connect } from 'react-redux'
import View from './SignInView'
import { actionCreators } from 'reducers/auth'

function mapStateToProps({ auth: { entities: { signIn: data } } }) {
  return { data }
}

export default connect(mapStateToProps, actionCreators)(View)