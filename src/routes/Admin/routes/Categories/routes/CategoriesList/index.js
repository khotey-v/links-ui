import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import CategoriesListContainer from './container/CategoriesListContainer'

export default class CategoriesListLayout extends Component {
  render() {
    return <div>
      <h2>Categories</h2>
      <RaisedButton label={<Link to="/admin/categories/create">Create</Link>} primary={true} />
      <Divider style={{ marginTop: 20 }} />
      <div>
        <CategoriesListContainer />
      </div>
    </div>
  }
}
