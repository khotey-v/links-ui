import { connect } from 'react-redux'
import { actionCreators } from 'reducers/categories'

import CategoryUpdateView from '../components/CategoryUpdateView'

function mapStateToProps({ categories: { entities : { form, category, categories } } }) {
  return { form, category, categories }
}

export default connect(mapStateToProps, actionCreators)(CategoryUpdateView);