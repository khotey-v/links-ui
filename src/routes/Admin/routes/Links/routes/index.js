import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LinksList from './LinksList'
import LinkCreate from './LinkCreate'
import LinkUpdate from './LinkUpdate'

export default <div>
  <Switch>
    <Route exact path="/admin/links" component={LinksList} />
    <Route path="/admin/links/create" component={LinkCreate} />
    <Route path="/admin/links/update/:url" component={LinkUpdate} />
  </Switch>
</div>
