import React from 'react'
import { Route, Switch } from 'react-router-dom'
import CategoriesList from './CategoriesList'
import CategoryCreate from './CategoryCreate'
import CategoryUpdate from './CategoryUpdate'

export default <div>
  <Switch>
    <Route exact path="/admin/categories" component={CategoriesList} />
    <Route path="/admin/categories/create" component={CategoryCreate} />
    <Route path="/admin/categories/update/:url" component={CategoryUpdate} />
  </Switch>
</div>
