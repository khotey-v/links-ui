import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import CategoryCreateContainer from './container/CategoryCreateContainer'

export default class CategoryCreateRoute extends Component {
  render() {
    return <div>
      <h2>Categories</h2>
      <h3>Create</h3>
      <RaisedButton label={<Link to="/admin/categories">Back</Link>} default={true} />
      <Divider style={{ marginTop: 20 }} />
      <div>
        <CategoryCreateContainer />
      </div>
    </div>
  }
}
