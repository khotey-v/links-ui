import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import LinkUpdateContainer from './container/LinkUpdateContainer'

export default class LinkUpdateRoute extends Component {
  render() {
    return <div>
      <h2>Links</h2>
      <h3>Update</h3>
      <RaisedButton label={<Link to="/admin/links">Back</Link>} default={true} />
      <Divider style={{ marginTop: 20 }} />
      <div>
        <LinkUpdateContainer />
      </div>
    </div>
  }
}
