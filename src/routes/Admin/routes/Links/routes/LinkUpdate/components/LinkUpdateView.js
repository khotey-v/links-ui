import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import LinkForm from '../../../components/LinkForm'

class LinkUpdateView extends Component {
  componentDidMount() {
    this.props.fetchLink(this.props.match.params.url)
    this.props.fetchCategories({ parent: 'yes' });
  }

  componentWillUnmount() {
    this.props.clearFormData()
  }

  onSubmit = data => {
    this.props.updateLink(data)
  };

  render() {
    const {
      form,
      link: {
        isLoading,
        error,
        data,
        receivedAt,
      },
      categories: {
        isLoading: categoriesIsLoading,
        error: categoriesError,
        data: categories,
      },
    } = this.props;

    if (isLoading) {
      return <div>Link with url {this.props.match.params.url} loading...</div>
    }

    if (!Object.keys(data).length && !receivedAt) {
      return <div>Link with url {this.props.match.params.url} not found ;(</div>
    }

    if (error) {
      return <div className="alert alert-error">{error}</div>
    }

    if (categoriesIsLoading) {
      return <div>Categories are loading...</div>
    }

    if (categoriesError) {
      return <div>{categoriesError}</div>
    }

    return <LinkForm buttonLabel="Update" formData={form} initialValues={data} categories={categories} onSubmit={this.onSubmit} />
  }
}

export default withRouter(LinkUpdateView)