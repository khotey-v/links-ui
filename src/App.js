import React from 'react'
import routes from './routes'

export default ({ store }) => {
  return <div>{routes(store)}</div>
}
