import React, { Component } from 'react'
import LinkForm from '../../../components/LinkForm'

export default class LinkCreateView extends Component {
  componentDidMount() {
    this.props.fetchCategories({ parent: 'yes' });
  }

  onSubmit = data => {
    this.props.createLink(data)
  };

  componentWillUnmount() {
    this.props.clearFormData()
  }

  render() {
    const { form, categories: { isLoading, error, data } } = this.props;

    if (isLoading) {
      return <div>Categories Loading...</div>
    }

    if (error) {
      return <div className="col-md-12">{error}</div>
    }

    return <LinkForm buttonLabel="Create" formData={form} categories={data} onSubmit={this.onSubmit} />
  }
}