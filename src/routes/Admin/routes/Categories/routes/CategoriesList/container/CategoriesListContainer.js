import { connect } from 'react-redux'
import { actionCreators } from 'reducers/categories'
import CategoriesListView from '../components/CategoriesListView'

function mapStateToProps({ categories: { entities: { categories }} }) {
  return { categories }
}

export default connect(mapStateToProps, actionCreators)(CategoriesListView)