import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LinksList from './containers/LinksList'

export default <Switch>
  <Route exact path="/competition/:url" component={LinksList} />
</Switch>
