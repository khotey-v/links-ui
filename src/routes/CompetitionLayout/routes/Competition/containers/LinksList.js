import { connect } from 'react-redux'
import LinksView from '../components/LinksView'
import { actionCreators } from 'reducers/links'

function mapStateToProps({ links }) {
  return { links }
}

export default connect(mapStateToProps, actionCreators)(LinksView)