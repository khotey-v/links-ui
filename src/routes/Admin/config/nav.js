export default [
  {
    name: 'Categories',
    path: '/admin/categories',
  },
  {
    name: 'Links',
    path: '/admin/links',
  },
  {
    name: 'Logout',
    path: '/logout'
  }
]