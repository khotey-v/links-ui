import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'

class SignInView extends Component {
  signIn(data) {
    this.props.signIn(data)
  }

  render() {
    const { handleSubmit, data: { error, receivedAt, isLoading } } = this.props;

    return <div>
      <div className="body" />
      <div className="grad" />
      <div className="header">
        <div>Site<span>ofCompetencies</span></div>
        <br />
        <div>Sign In</div>
      </div>
      <br />
      <div className="login">
        {
          receivedAt && error && <div>{error}</div>
        }
        <form onSubmit={handleSubmit(::this.signIn)}>
          <Field component="input" type="text" placeholder="username" name="login" /><br />
          <Field component="input" type="password" placeholder="password" name="password" /><br />
          <button disabled={isLoading}>Sign In</button>
        </form>
      </div>
    </div>
  }
}

export default reduxForm({
  form: 'sign-in'
})(SignInView)