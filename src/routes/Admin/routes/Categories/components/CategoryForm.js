import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';

class CreateCategoryView extends Component {

  renderCategoriesOptions() {
    const { categories } = this.props;

    return categories.map(category => <MenuItem key={category._id} value={category._id} primaryText={category.name} />)
  }

  render() {
    const { handleSubmit, onSubmit, formData: { error, isLoading, receivedAt }, buttonLabel } = this.props;

    return <form onSubmit={handleSubmit(onSubmit)} className="row">
        {
          receivedAt && error && <div className="col-md-12">{error}</div>
        }
        <Field className="col-md-12" label="Name" style={{ width: '100%' }} component={({input, label, meta: {touched, error}, ...custom}) => (
          <TextField
            hintText={label}
            {...input}
            {...custom}
          />
        )} name="name" />
        <Field className="col-md-12" label="Url" style={{ width: '100%' }} component={({input, label, meta: {touched, error}, ...custom}) => (
          <TextField
            hintText={label}
            {...input}
            {...custom}
          />
        )} name="url" />
        <Field className="col-md-12" label="Parent Category" style={{ width: '100%' }} component={
          ({ input, label, meta: {touched, error}, children, ...custom }) => (
            <SelectField
              errorText={touched && error}
              {...input}
              onChange={(event, index, value) => input.onChange(value)}
              children={children}
              {...custom}
            />
        )}
         name="parent"
         value={this.props.initialValues ? this.props.initialValues._id : null}
        >
          <MenuItem value="" primaryText="Select parent category" />
          {this.renderCategoriesOptions()}
        </Field>
        <div className="col-md-12">
          <button disabled={isLoading} className="btn btn-primary">{buttonLabel}</button>
        </div>
      </form>
  }
}

export default reduxForm({ form: 'category-form' })(CreateCategoryView)