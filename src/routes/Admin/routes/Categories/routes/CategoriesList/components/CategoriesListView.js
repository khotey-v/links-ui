import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';

export default class CategoriesListView extends Component {
  componentDidMount() {
    this.props.fetchCategories();
  }

  renderCategories() {
    const { categories : { data } } = this.props;

    return <Table>
      <TableHeader>
        <TableRow>
          <TableHeaderColumn>ID</TableHeaderColumn>
          <TableHeaderColumn>Name</TableHeaderColumn>
          <TableHeaderColumn>Url</TableHeaderColumn>
          <TableHeaderColumn>Parent Category</TableHeaderColumn>
          <TableHeaderColumn>Actions</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody>
        {
          data.map((category, index) => <TableRow key={category._id}>
            <TableRowColumn>{category._id}</TableRowColumn>
            <TableRowColumn>{category.name}</TableRowColumn>
            <TableRowColumn>{category.url}</TableRowColumn>
            <TableRowColumn>{category.parent ? category.parent.name : '-'}</TableRowColumn>
            <TableRowColumn>
              <IconMenu
                iconButtonElement={
                  <IconButton touch={true}>
                    <NavigationExpandMoreIcon />
                  </IconButton>
                }
              >
                <MenuItem primaryText={<Link to={`/admin/categories/update/${category.url}`}>Update</Link>} />
                <MenuItem primaryText="Delete" onTouchTap={e => this.props.deleteCategory(category.url, index)} />
              </IconMenu>
            </TableRowColumn>
          </TableRow>)
        }
      </TableBody>
    </Table>
  }

  render() {
    const { isLoading, error, receivedAt, data } = this.props.categories

    if (isLoading) {
      return <div>Categories Loading...</div>
    }

    if (receivedAt && !data.length) {
      return <div>No Categories</div>
    }

    return <div>
      {
        error && <div>{error}</div>
      }
      {this.renderCategories()}
    </div>
  }
}