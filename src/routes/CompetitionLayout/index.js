import React from 'react'
import { connect } from 'react-redux'
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom'
import { actionCreators } from 'reducers/categories';
import CompetitionRoutes from './routes/Competition'

injectTapEventPlugin();

class CompetitionLayout extends React.Component {
  componentDidMount() {
    this.props.fetchCategories({ parent: 'no' });
    this.props.fetchCategoriesWithParent({ parent: 'yes' });
  }

  renderCategories() {
    const { categories: { entities: { categories } } } = this.props;

    return categories.data.map(category => {
      return <DropDownMenu value={0}>
        <MenuItem value={0} primaryText={category.name}/>
        {this.renderCategoriesByParent(category._id)}
      </DropDownMenu>
    })
  }

  renderCategoriesByParent(parentId) {
    const { categories: { entities: { categoriesWithParent } } } = this.props;
    const filteredCategories = categoriesWithParent.data.filter(category => category.parent._id === parentId)

    return filteredCategories.map(category => {
      return <MenuItem children={<Link to={`/competition/${category.url}`}>{category.name}</Link>} />
    })
  }

  render() {
    return <MuiThemeProvider>
      <div>
        <div>
          {this.renderCategories()}
        </div>
        <div>
          {CompetitionRoutes}
        </div>
      </div>
    </MuiThemeProvider>
  }
}

function mapStateToProps({ categories }) {
  return { categories }
}

export default connect(mapStateToProps, actionCreators)(CompetitionLayout)