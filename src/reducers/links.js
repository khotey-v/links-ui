import axios from 'axios'
// TODO: add redirect on login sucess
import createRequestAction from 'utils/createRequestAction'
import timestamp from 'utils/timestamp'
import api from 'config/api'

const initState = {
  entities: {
    form : {
      error: null,
      receivedAt: null,
      isLoading: false,
    },
    links : {
      error: null,
      receivedAt: null,
      isLoading: false,
      data: [],
    },
    deleteLink : {
      error: null,
      receivedAt: null,
      isLoading: false,
    },
    link : {
      error: null,
      receivedAt: null,
      isLoading: false,
      data: {},
    },
  },
};

const KEY = 'links'
const CREATE = createRequestAction(`${KEY}/create`)
const FETCH_LINKS = createRequestAction(`${KEY}/fetch-links`)
const FETCH_LINK = createRequestAction(`${KEY}/fetch-link`)
const DELETE_LINK = createRequestAction(`${KEY}/delete-link`)
const UPDATE_LINK = createRequestAction(`${KEY}/update-link`)
const CLEAR_FORM_DATA = `${KEY}/clear-form-data`
const CLEAR_LINKS = `${KEY}/clear-links`

function clearLinks() {
  return {
    type: CLEAR_LINKS,
  }
}

function createLink(data) {
  return dispatch => {
    dispatch({
      type: CREATE.REQUEST,
    });

    axios
      .post(`${api.url}/links`, data)
      .then(data => {
        dispatch({
          type: CREATE.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        const error = e.response.data.message

        dispatch({
          type: CREATE.FAILURE,
          payload: error
        })
      })
  }
}

function updateLink(data) {
  return dispatch => {
    dispatch({
      type: UPDATE_LINK.REQUEST,
    });

    axios
      .put(`${api.url}/links`, data)
      .then(data => {
        dispatch({
          type: UPDATE_LINK.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        const error = e.response.data.message

        dispatch({
          type: UPDATE_LINK.FAILURE,
          payload: error
        })
      })
  }
}

function fetchLinks() {
  return dispatch => {
    dispatch({
      type: FETCH_LINKS.REQUEST,
    });

    axios
      .get(`${api.url}/links`)
      .then(({ data }) => {
        dispatch({
          type: FETCH_LINKS.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: FETCH_LINKS.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function fetchLink(url) {
  return dispatch => {
    dispatch({
      type: FETCH_LINK.REQUEST,
    });

    axios
      .get(`${api.url}/links/${url}`)
      .then(({ data }) => {
        dispatch({
          type: FETCH_LINK.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: FETCH_LINK.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function fetchLinksByCategoryUrl(url) {
  return dispatch => {
    dispatch({
      type: FETCH_LINKS.REQUEST,
    });

    axios
      .get(`${api.url}/categories/${url}/links`)
      .then(({ data }) => {
        dispatch({
          type: FETCH_LINKS.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: FETCH_LINKS.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function deleteLink(id, index) {
  return dispatch => {
    dispatch({
      type: DELETE_LINK.REQUEST,
    });

    axios
      .delete(`${api.url}/links/${id}`)
      .then(() => {
        dispatch({
          type: DELETE_LINK.SUCCESS,
          payload: { id, index },
        })
      })
      .catch(e => {
        dispatch({
          type: DELETE_LINK.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function clearFormData() {
  return {
    type: CLEAR_FORM_DATA,
  }
}

const actionHandlers = {
  [CREATE.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        isLoading: true,
        error: null,
      }
    }
  }),
  [CREATE.SUCCESS]: (state, action) => {
    location.pathname = '/admin/links'

    return {
      ...state,
      entities: {
        ...state.entities,
        form: {
          ...state.entities.form,
          receivedAt: timestamp(),
          isLoading: false,
        }
      }
    }
  },
  [CREATE.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [UPDATE_LINK.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        isLoading: true,
        error: null,
      }
    }
  }),
  [UPDATE_LINK.SUCCESS]: (state, action) => {
    location.pathname = '/admin/links'

    return {
      ...state,
      entities: {
        ...state.entities,
        form: {
          ...state.entities.form,
          receivedAt: timestamp(),
          isLoading: false,
        }
      }
    }
  },
  [UPDATE_LINK.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [FETCH_LINKS.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      links: {
        ...state.entities.links,
        isLoading: true,
        error: null,
      }
    }
  }),
  [FETCH_LINKS.SUCCESS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      links: {
        ...state.entities.links,
        data: action.payload,
        receivedAt: timestamp(),
        isLoading: false,
      }
    }
  }),
  [FETCH_LINKS.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      links: {
        ...state.entities.links,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [FETCH_LINK.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      link: {
        ...state.entities.link,
        isLoading: true,
        error: null,
      }
    }
  }),
  [FETCH_LINK.SUCCESS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      link: {
        ...state.entities.link,
        data: action.payload,
        receivedAt: timestamp(),
        isLoading: false,
      }
    }
  }),
  [FETCH_LINK.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      link: {
        ...state.entities.link,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [DELETE_LINK.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      deleteLink: {
        ...state.entities.deleteLink,
        isLoading: true,
        error: null,
      }
    }
  }),
  [DELETE_LINK.SUCCESS]: (state, action) => {
    const links = [ ...state.entities.links.data ]
    const { index } = action.payload

    links.splice(index, 1)

    return {
      ...state,
      entities: {
        ...state.entities,
        deleteLink: {
          ...state.entities.deleteLink,
          receivedAt: timestamp(),
          isLoading: false,
        },
        links: {
          ...state.entities.links,
          data: links,
        }
      }
    }
  },
  [DELETE_LINK.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      deleteLink: {
        ...state.entities.deleteLink,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [CLEAR_LINKS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      links: {
        ...state.entities.links,
        data: [],
      }
    }
  }),
}

const actionCreators = {
  createLink,
  fetchLinks,
  clearFormData,
  fetchLink,
  deleteLink,
  updateLink,
  fetchLinksByCategoryUrl,
  clearLinks,
}

const actionTypes = {
  CREATE,
  FETCH_LINKS,
  FETCH_LINK,
  DELETE_LINK,
  CLEAR_FORM_DATA,
  UPDATE_LINK,
  CLEAR_LINKS
}

export {
  actionTypes,
  actionCreators,
  initState,
}

export default (state = initState, action) => {
  const handler = actionHandlers[action.type]

  return handler ? handler(state, action) : state
}