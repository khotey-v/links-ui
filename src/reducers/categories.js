import axios from 'axios'
// TODO: add redirect on login sucess
import createRequestAction from 'utils/createRequestAction'
import timestamp from 'utils/timestamp'
import api from 'config/api'

const initState = {
  entities: {
    form : {
      error: null,
      receivedAt: null,
      isLoading: false,
    },
    deleteCategory : {
      error: null,
      receivedAt: null,
      isLoading: false,
    },
    categories : {
      error: null,
      receivedAt: null,
      isLoading: false,
      data: [],
    },
    categoriesWithParent : {
      error: null,
      receivedAt: null,
      isLoading: false,
      data: [],
    },
    category : {
      error: null,
      receivedAt: null,
      isLoading: false,
      data: {},
    },
  },
};

const KEY = 'categories'
const CREATE = createRequestAction(`${KEY}/create`)
const FETCH_CATEGORIES = createRequestAction(`${KEY}/fetch-categories`)
const FETCH_CATEGORIES_WITH_PARENT = createRequestAction(`${KEY}/fetch-categories-with-parent`)
const FETCH_CATEGORY = createRequestAction(`${KEY}/fetch-category`)
const DELETE_CATEGORY = createRequestAction(`${KEY}/delete-category`)
const UPDATE_CATEGORY = createRequestAction(`${KEY}/update-category`)
const CLEAR_FORM_DATA = `${KEY}/clear-form-data`

function createCategory(data) {
  return dispatch => {
    dispatch({
      type: CREATE.REQUEST,
    });

    axios
      .post(`${api.url}/categories`, data)
      .then(data => {
        dispatch({
          type: CREATE.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        const error = e.response.data.message

        dispatch({
          type: CREATE.FAILURE,
          payload: error
        })
      })
  }
}

function updateCategory(data) {
  return dispatch => {
    dispatch({
      type: UPDATE_CATEGORY.REQUEST,
    });

    axios
      .put(`${api.url}/categories`, data)
      .then(data => {
        dispatch({
          type: UPDATE_CATEGORY.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        const error = e.response.data.message

        dispatch({
          type: UPDATE_CATEGORY.FAILURE,
          payload: error
        })
      })
  }
}

function fetchCategories(params = {}) {
  return dispatch => {
    dispatch({
      type: FETCH_CATEGORIES.REQUEST,
    });

    axios
      .get(`${api.url}/categories`, { params })
      .then(({ data }) => {
        dispatch({
          type: FETCH_CATEGORIES.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: FETCH_CATEGORIES.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function fetchCategoriesWithParent(params = {}) {
  return dispatch => {
    dispatch({
      type: FETCH_CATEGORIES.REQUEST,
    });

    axios
      .get(`${api.url}/categories`, { params })
      .then(({ data }) => {
        dispatch({
          type: FETCH_CATEGORIES_WITH_PARENT.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: FETCH_CATEGORIES.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function fetchCategory(url) {
  return dispatch => {
    dispatch({
      type: FETCH_CATEGORY.REQUEST,
    });

    axios
      .get(`${api.url}/categories/${url}`)
      .then(({ data }) => {
        dispatch({
          type: FETCH_CATEGORY.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: FETCH_CATEGORY.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function deleteCategory(id, index) {
  return dispatch => {
    dispatch({
      type: DELETE_CATEGORY.REQUEST,
    });

    axios
      .delete(`${api.url}/categories/${id}`)
      .then(() => {
        dispatch({
          type: DELETE_CATEGORY.SUCCESS,
          payload: { id, index },
        })
      })
      .catch(e => {
        dispatch({
          type: DELETE_CATEGORY.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function clearFormData() {
  return {
    type: CLEAR_FORM_DATA,
  }
}

const actionHandlers = {
  [CREATE.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        isLoading: true,
        error: null,
      }
    }
  }),
  [CREATE.SUCCESS]: (state, action) => {
    location.pathname = '/admin/categories'

    return {
      ...state,
      entities: {
        ...state.entities,
        form: {
          ...state.entities.form,
          receivedAt: timestamp(),
          isLoading: false,
        }
      }
    }
  },
  [CREATE.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [UPDATE_CATEGORY.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        isLoading: true,
        error: null,
      }
    }
  }),
  [UPDATE_CATEGORY.SUCCESS]: (state, action) => {
    location.pathname = '/admin/categories'

    return {
      ...state,
      entities: {
        ...state.entities,
        form: {
          ...state.entities.form,
          receivedAt: timestamp(),
          isLoading: false,
        }
      }
    }
  },
  [UPDATE_CATEGORY.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      form: {
        ...state.entities.form,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [FETCH_CATEGORIES.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      categories: {
        ...state.entities.categories,
        isLoading: true,
        error: null,
      }
    }
  }),
  [FETCH_CATEGORIES.SUCCESS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      categories: {
        ...state.entities.categories,
        data: action.payload,
        receivedAt: timestamp(),
        isLoading: false,
      }
    }
  }),
  [FETCH_CATEGORIES.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      categories: {
        ...state.entities.categories,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [FETCH_CATEGORY.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      category: {
        ...state.entities.category,
        isLoading: true,
        error: null,
      }
    }
  }),
  [FETCH_CATEGORY.SUCCESS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      category: {
        ...state.entities.category,
        data: action.payload,
        receivedAt: timestamp(),
        isLoading: false,
      }
    }
  }),
  [FETCH_CATEGORY.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      category: {
        ...state.entities.category,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [DELETE_CATEGORY.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      deleteCategory: {
        ...state.entities.deleteCategory,
        isLoading: true,
        error: null,
      }
    }
  }),
  [DELETE_CATEGORY.SUCCESS]: (state, action) => {
    const categories = [ ...state.entities.categories.data ]
    const { index } = action.payload

    categories.splice(index, 1)

    return {
      ...state,
      entities: {
        ...state.entities,
        deleteCategory: {
          ...state.entities.deleteCategory,
          receivedAt: timestamp(),
          isLoading: false,
        },
        categories: {
          ...state.entities.categories,
          data: categories
        }
      }
    }
  },
  [DELETE_CATEGORY.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      deleteCategory: {
        ...state.entities.deleteCategory,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [FETCH_CATEGORIES_WITH_PARENT.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      categoriesWithParent: {
        ...state.entities.categoriesWithParent,
        isLoading: true,
        error: null,
      }
    }
  }),
  [FETCH_CATEGORIES_WITH_PARENT.SUCCESS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      categoriesWithParent: {
        ...state.entities.categoriesWithParent,
        data: action.payload,
        receivedAt: timestamp(),
        isLoading: false,
      }
    }
  }),
  [FETCH_CATEGORIES_WITH_PARENT.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      categoriesWithParent: {
        ...state.entities.categoriesWithParent,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),
}

const actionCreators = {
  createCategory,
  fetchCategories,
  clearFormData,
  deleteCategory,
  fetchCategory,
  updateCategory,
  fetchCategoriesWithParent,
}

const actionTypes = {
  CREATE,
  FETCH_CATEGORIES,
  FETCH_CATEGORY,
  DELETE_CATEGORY,
  CLEAR_FORM_DATA,
  UPDATE_CATEGORY,
  FETCH_CATEGORIES_WITH_PARENT,
}

export {
  actionTypes,
  actionCreators,
  initState,
}

export default (state = initState, action) => {
  const handler = actionHandlers[action.type]

  return handler ? handler(state, action) : state
}