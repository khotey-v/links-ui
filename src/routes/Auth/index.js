import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import SignIn from './SignIn'
import SignUp from './SignUp'
import './css/auth.css'

class Auth extends Component {
  componentDidMount() {
    const { auth: { isLogged } } = this.props;

    if (isLogged) {
      location.replace('/')
    }
  }

  render() {
    return <div>
      <Route path="/auth/sign-in" component={SignIn} />
      <Route path="/auth/sign-up" component={SignUp} />
    </div>
  }
}

function mapStateToProps({ auth }) {
  return { auth }
}

export default connect(mapStateToProps)(Auth)


