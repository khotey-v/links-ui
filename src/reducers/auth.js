import axios from 'axios'
import cookie from 'js-cookie'
// TODO: add redirect on login sucess
import createRequestAction from 'utils/createRequestAction'
import timestamp from 'utils/timestamp'
import api from 'config/api'

const initState = {
  isLogged: localStorage.isLogged || false,
  entities: {
    user: {
      error: null,
      receivedAt: null,
      data: {},
    },
    signUp: {
      error: null,
      receivedAt: null,
      isLoading: false,
    },
    signIn: {
      error: null,
      receivedAt: null,
      isLoading: false,
    }
  }
};

const KEY = 'auth'
const SIGN_UP = createRequestAction(`${KEY}/sign-up`)
const SIGN_IN= createRequestAction(`${KEY}/sign-in`)
const LOGOUT = `${KEY}/logout`

function signUp(data) {
  return dispatch => {
    dispatch({
      type: SIGN_UP.REQUEST,
    });

    axios
      .post(`${api.url}/signup`, data)
      .then(data => {
        dispatch({
          type: SIGN_UP.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        dispatch({
          type: SIGN_UP.FAILURE,
          payload: e.response.data.message
        })
      })
  }
}

function signIn(data) {
  return dispatch => {
    dispatch({
      type: SIGN_IN.REQUEST,
    });

    axios
      .post(`${api.url}/signin`, data)
      .then(({ data }) => {
        dispatch({
          type: SIGN_IN.SUCCESS,
          payload: data,
        })
      })
      .catch(e => {
        const error = JSON.parse(JSON.stringify(e))

        dispatch({
          type: SIGN_IN.FAILURE,
          payload: error.response.data.message
        })
      })
  }
}

function logout() {
  return {
    type: LOGOUT,
  };
}

const actionHandlers = {
  [SIGN_UP.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signUp: {
        ...state.signIn,
        isLoading: true,
        error: null,
      }
    }
  }),
  [SIGN_UP.SUCCESS]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signUp: {
        ...state.entities.signUp,
        receivedAt: timestamp(),
        isLoading: false,
      }
    }
  }),
  [SIGN_UP.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signUp: {
        ...state.entities.signUp,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [SIGN_IN.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signIn: {
        ...state.signIn,
        isLoading: true,
        error: null,
      }
    }
  }),
  [SIGN_IN.SUCCESS]: (state, action) => {
    localStorage.setItem('isLogged', true)
    cookie.set('XSRF-TOKEN', action.payload, { path: '' })
    location.replace('/admin')

    return {
      ...state,
      isLogged: true,
      entities: {
        ...state.entities,
        signIn: {
          ...state.entities.signIn,
          receivedAt: timestamp(),
          isLoading: false,
        }
      }
    }
  },
  [SIGN_IN.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signIn: {
        ...state.entities.signIn,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),

  [LOGOUT]: state => ({
    ...initState,
    isLogged: false,
  })
}

const actionCreators = {
  signUp,
  signIn,
  logout,
}

const actionTypes = {
  SIGN_UP,
  SIGN_IN,
  LOGOUT,
}

export {
  actionTypes,
  actionCreators,
  initState,
}

export default (state = initState, action) => {
  const handler = actionHandlers[action.type]

  return handler ? handler(state, action) : state
}