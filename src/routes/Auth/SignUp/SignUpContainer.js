import React from 'react'
import { connect } from 'react-redux'
import View from './SignUpView'
import { actionCreators } from 'reducers/auth'

function mapStateToProps({ auth: { entities: { signUp: data } } }) {
  return { data }
}

export default connect(mapStateToProps, actionCreators)(View)