import { connect } from 'react-redux'
import { actionCreators } from 'reducers/links'
import { actionCreators as categoriesActionCreators } from 'reducers/categories'
import LinkCreateView from '../components/LinkCreateView'

function mapStateToProps({ links: { entities: { form, link } }, categories: { entities: { categories } } }) {
  return { form, link, categories }
}

export default connect(mapStateToProps, { ...actionCreators, ...categoriesActionCreators })(LinkCreateView)