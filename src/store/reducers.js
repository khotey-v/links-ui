import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'

import reducers from '../reducers'

export const makeRootReducer = () => combineReducers({
  form,
  ...reducers,
})

export default makeRootReducer
