import React, { Component } from 'react'
import CategoryForm from '../../../components/CategoryForm'

export default class CategoryCreateView extends Component {
  componentDidMount() {
    this.props.fetchCategories({ parent: 'no' })
  }

  onSubmit = data => {
    this.props.createCategory(data)
  };

  componentWillUnmount() {
    this.props.clearFormData()
  }

  render() {
    const { form, categories: { isLoading, error, data } } = this.props;

    if (isLoading) {
      return <div>Categories Loading...</div>
    }

    if (error) {
      return <div className="col-md-12">{error}</div>
    }

    return <CategoryForm buttonLabel="Create" formData={form} categories={data} onSubmit={this.onSubmit} />
  }
}