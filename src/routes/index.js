import React from 'react'
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom'
import axios from 'axios'
import AdminLayout from './Admin'
import AuthRoutes from './Auth'
import CompetitionLayout from './CompetitionLayout'
import cookie from 'js-cookie'
import { actionCreators as authActionCreators } from 'reducers/auth'

axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  error = JSON.parse(JSON.stringify(error))
  const { response: { status } } = error;

  if (status === 401) {
    cookie.remove('XSRF-TOKEN', { path: '' })
    localStorage.removeItem('isLogged')

    location.pathname = '/sign-in'
  }

  return Promise.reject(error);
});

axios.interceptors.request.use(function (config) {
  return { ...config, headers: { 'XSRF-TOKEN': cookie.get('XSRF-TOKEN') } };
}, function (error) {
  return Promise.reject(error);
});


export default store => {

  return (
    <BrowserRouter basepath="/">
      <Switch>
        <Route path="/admin" component={AdminLayout} />
        <Route path="/logout" render={() => {
          store.dispatch(authActionCreators.logout())
          cookie.remove('XSRF-TOKEN')
          localStorage.removeItem('isLogged')

          location.replace('/')
        }} />
        <Route path="/auth" component={AuthRoutes} />
        <Route path="/" component={CompetitionLayout} />
      </Switch>
    </BrowserRouter>
  );
}