import React from 'react'
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Route, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import navigation from './config/nav'
import CategoriesRoutes from './routes/Categories'
import LinksRoutes from './routes/Links'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';

import { List, ListItem } from 'material-ui/List';

class Admin extends React.Component {
  componentDidMount() {
    const { auth: { isLogged } } = this.props;

    if (!isLogged) {
      location.replace('/auth/sign-in')
    }
  }

  render() {
    return <MuiThemeProvider>
      <div className="row">
        <AppBar
          title={<Link to="/admin">Admin</Link>}
        />
        <div className="col-md-4 col-sm-1" style={{ height: '100%' }}>
          <List>
            {
              navigation.map(nav => <ListItem key={nav.path} primaryText={<Link to={nav.path}>{nav.name}</Link>}/>)
            }
          </List>
        </div>
        <div className="col-sm-1 col-md-8">
          {CategoriesRoutes}
          {LinksRoutes}
        </div>
      </div>
    </MuiThemeProvider>
  }
}

function mapStateToProps({ auth }) {
  return { auth }
}

export default connect(mapStateToProps)(Admin)