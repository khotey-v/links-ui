import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import promiseMiddleware from 'redux-promise';
import createReducer from './reducers'

// TODO: create inject reducer function for local reducers
const middlewares = [
  thunk,
  createLogger(),
  promiseMiddleware,
]
const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore)

export default (initialState = {}) => (createStoreWithMiddleware(createReducer(), initialState))
