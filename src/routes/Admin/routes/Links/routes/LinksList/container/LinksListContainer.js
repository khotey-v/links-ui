import { connect } from 'react-redux'
import { actionCreators } from 'reducers/links'
import LinksListView from '../components/LinksListView'

function mapStateToProps({ links: { entities: { links }} }) {
  return { links }
}

export default connect(mapStateToProps, actionCreators)(LinksListView)