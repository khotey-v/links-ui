import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';

export default class LinksListView extends Component {
  componentDidMount() {
    this.props.fetchLinks();
  }

  renderLinks() {
    const { links : { data } } = this.props;

    return <Table>
      <TableHeader>
        <TableRow>
          <TableHeaderColumn>ID</TableHeaderColumn>
          <TableHeaderColumn>Name</TableHeaderColumn>
          <TableHeaderColumn>Url</TableHeaderColumn>
          <TableHeaderColumn>Category</TableHeaderColumn>
          <TableHeaderColumn>Actions</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody>
        {
          data.map((link, index) => <TableRow key={link._id}>
            <TableRowColumn>{link._id}</TableRowColumn>
            <TableRowColumn>{link.name}</TableRowColumn>
            <TableRowColumn>{link.url}</TableRowColumn>
            <TableRowColumn>{link.category ? link.category.name: '-'}</TableRowColumn>
            <TableRowColumn>
              <IconMenu
                iconButtonElement={
                  <IconButton touch={true}>
                    <NavigationExpandMoreIcon />
                  </IconButton>
                }
              >
                <MenuItem primaryText={<Link to={`/admin/links/update/${link.url}`}>Update</Link>} />
                <MenuItem primaryText="Delete" onTouchTap={e => this.props.deleteLink(link.url, index)} />
              </IconMenu>
            </TableRowColumn>
          </TableRow>)
        }
      </TableBody>
    </Table>
  }

  render() {
    const { isLoading, error, receivedAt, data } = this.props.links

    if (isLoading) {
      return <div>Links Loading...</div>
    }

    if (receivedAt && !data.length) {
      return <div>No Links</div>
    }

    return <div>
      {
        error && <div>{error}</div>
      }
      {this.renderLinks()}
    </div>
  }
}