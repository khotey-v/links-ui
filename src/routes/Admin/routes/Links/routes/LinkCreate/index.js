import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import LinkCreateContainer from './container/LinkCreateContainer'

export default class LinkCreateRoute extends Component {
  render() {
    return <div>
      <h2>Links</h2>
      <h3>Create</h3>
      <RaisedButton label={<Link to="/admin/links">Back</Link>} default={true} />
      <Divider style={{ marginTop: 20 }} />
      <div>
        <LinkCreateContainer />
      </div>
    </div>
  }
}
