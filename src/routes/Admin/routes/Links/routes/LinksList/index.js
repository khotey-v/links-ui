import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import LinksListContainer from './container/LinksListContainer'

export default class LinksListLayout extends Component {
  render() {
    return <div>
      <h2>Links</h2>
      <RaisedButton label={<Link to="/admin/links/create">Create</Link>} primary={true} />
      <Divider style={{ marginTop: 20 }} />
      <div>
        <LinksListContainer />
      </div>
    </div>
  }
}
