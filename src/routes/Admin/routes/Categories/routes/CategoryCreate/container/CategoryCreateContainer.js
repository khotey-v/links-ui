import { connect } from 'react-redux'
import { actionCreators } from 'reducers/categories'
import CategoryCreateView from '../components/CategoryCreateView'

function mapStateToProps({ categories: { entities: { form, categories }} }) {
  return { form, categories }
}

export default connect(mapStateToProps, actionCreators)(CategoryCreateView)