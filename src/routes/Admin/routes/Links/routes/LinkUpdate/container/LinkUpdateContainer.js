import { connect } from 'react-redux'
import { actionCreators } from 'reducers/links'
import { actionCreators as categoriesActionCreators } from 'reducers/categories'
import LinkUpdateView from '../components/LinkUpdateView'

function mapStateToProps({ links: { entities: { form, link } }, categories: { entities: { categories } } }) {
  return { form, link, categories }
}

export default connect(mapStateToProps, { ...actionCreators, ...categoriesActionCreators })(LinkUpdateView)