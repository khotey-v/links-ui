import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class CreateLinkView extends Component {

  renderLinksOptions() {
    const { categories } = this.props;

    return categories.map(category => <MenuItem key={category._id} value={category._id} primaryText={category.name} />)
  }

  render() {
    const { handleSubmit, onSubmit, formData: { error, isLoading, receivedAt }, buttonLabel } = this.props;

    return <form onSubmit={handleSubmit(onSubmit)} className="row">
        {
          receivedAt && error && <div className="col-md-12">{error}</div>
        }
        <Field className="col-md-12" label="Name" style={{ width: '100%' }} component={({input, label, meta: {touched, error}, ...custom}) => (
          <TextField
            hintText={label}
            {...input}
            {...custom}
          />
        )} name="name" />
        <Field className="col-md-12" label="Url" style={{ width: '100%' }} component={({input, label, meta: {touched, error}, ...custom}) => (
          <TextField
            hintText={label}
            {...input}
            {...custom}
          />
        )} name="url" />
        <Field className="col-md-12" label="Parent Link" style={{ width: '100%' }} component={
          ({ input, label, meta: {touched, error}, children, ...custom }) => (
            <SelectField
              errorText={touched && error}
              {...input}
              onChange={(event, index, value) => input.onChange(value)}
              children={children}
              {...custom}
            />
        )}
         name="category"
         value={this.props.initialValues ? this.props.initialValues._id : null}
        >
          <MenuItem value="" primaryText="Select parent link" />
          {this.renderLinksOptions()}
        </Field>
        <div className="col-md-12">
          {/*<RaisedButton disabled={isLoading} label="Create" primary={true} onClick={e => this.props.submit('category-form')} />*/}
          <button disabled={isLoading} className="btn btn-primary">{buttonLabel}</button>
        </div>
      </form>
  }
}

export default reduxForm({ form: 'category-form' })(CreateLinkView)