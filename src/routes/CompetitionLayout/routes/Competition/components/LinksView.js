import React from 'react'

export default class LinksView extends React.Component {
  componentDidMount() {
    this.props.fetchLinksByCategoryUrl(this.props.match.params.url)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.url !== this.props.match.params.url) {
      this.props.clearLinks();
      this.props.fetchLinksByCategoryUrl(nextProps.match.params.url)
    }
  }

  render() {
    const { links: { entities: { links } } } = this.props;
    console.log(links);

    return <div>
      {
        links.data.map(link => <div><a href={link.url}>{link.name}</a></div>)
      }
    </div>
  }
}