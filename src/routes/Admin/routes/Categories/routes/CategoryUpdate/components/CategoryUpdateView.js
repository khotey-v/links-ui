import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import CategoryForm from '../../../components/CategoryForm'

class CategoryUpdateView extends Component {
  componentDidMount() {
    this.props.fetchCategory(this.props.match.params.url)
    this.props.fetchCategories({ parent: 'no' })
  }

  componentWillUnmount() {
    this.props.clearFormData()
  }

  onSubmit = data => {
    this.props.updateCategory(data);
  };

  render() {
    const {
      form,
      category: {
        isLoading,
        error,
        data,
        receivedAt,
      },
      categories: {
        isLoading: categoriesIsLoading,
        error: categoriesError,
        data: categories,
      },
    } = this.props;

    if (isLoading) {
      return <div>Category with url {this.props.match.params.url} loading...</div>
    }

    if (!Object.keys(data).length && !receivedAt) {
      return <div>Category with url {this.props.match.params.url} not found ;(</div>
    }

    if (error) {
      return <div>{error}</div>
    }

    if (categoriesIsLoading) {
      return <div>Categories are loading...</div>
    }

    if (categoriesError) {
      return <div>{categoriesError}</div>
    }

    return <CategoryForm buttonLabel="Update" formData={form} initialValues={data} categories={categories.filter(category => category._id !== data._id )} onSubmit={this.onSubmit} />
  }
}

export default withRouter(CategoryUpdateView)